'use strict';

const config = require('../../configuration');
const httplease = require('httplease');
const httpleaseAsap = require('httplease-asap');

const jwtConfig = Object.assign({
    issuer: config.xpsearchUserSearcher.asap.issuer,
    audience: config.xpsearchUserSearcher.asap.audience
}, httpleaseAsap.parseDataUri(config.xpsearchUserSearcher.asap.keyDataUri));

/**
 * xpsearch-user-searcher REST client
 */
class XpsearchUserSearcherClient {

    constructor() {
        this.httpClient = httplease.builder()
            .withBaseUrl(config.xpsearchUserSearcher.baseUrl)
            .withBufferJsonResponseHandler()
            .withHeaders({
                'Content-Type': 'application/json'
            })
            .withTimeout(5000)
            .withFilter(httpleaseAsap.createJwtAuthFilter(jwtConfig));
    }

    async searchByDisplayName(user, cloudId) {
        console.log('[XpsearchUserSearcherCli] /document/search');
        const payload = {
            query: 'name.raw:"' + user.fullName + '"',
            limit: 10,
            principal : user.userId,
            scopes: [{
                cloudId: cloudId,
                productId: 'hipchat.cloud'
            }]
        };

        return await this.httpClient
            .withMethodPost()
            .withPath('/document/search')
            .withExpectStatus([200])
            .withJsonBody(payload)
            .send()
            .then((response) => (response.body));
    }

}

module.exports = {XpsearchUserSearcherClient};
