'use strict';

const q = require('q');
const config = require('../../../configuration');
const {XpsearchUserSearcherClient} = require('../../../lib/clients/xpsearch-user-searcher-client');

const {AidAccountRestClient} = require('../../../lib/clients/aid-account-rest-client');
const {AidManagementRestClient} = require('../../../lib/clients/aid-management-rest-client');
const {AidSignupRestClient} = require('../../../lib/clients/aid-signup-rest-client');

const xpsearchUserSearcherClient = new XpsearchUserSearcherClient();

const accountClient = new AidAccountRestClient();
const signupClient = new AidSignupRestClient({cookieName: config.cookieName});
const ten_minutes_milliseconds = 600000;

// -----------------------------------------------------------------------------
// This is the config required for this test
// -----------------------------------------------------------------------------
const USER_BASIC = config.userAccounts.basicUser;
const COOKIE_NAME = config.cookieName;
const CLOUD_ID = config.userAccounts.cloudId;
// -----------------------------------------------------------------------------
const LOG_PREFIX = '[XpsearchUserSearcherTest]';
let userWithOriginalName;
let aidManagementClient;
// variable to decide if we need to run the test
let bail = false;

const LOG = function(...args) {
    console.log(LOG_PREFIX, ...args);
};


describe('flows/xpsearch-user-searcher/search', () => {

    beforeEach(willResolve(async () => {
        const token = await accountClient.getAuthToken(USER_BASIC);
        const sessionCookie = (await signupClient.getAuthCookie({token: token})).split('=')[1];
        aidManagementClient = new AidManagementRestClient({
            sessionToken: sessionCookie,
            sessionCookieName: COOKIE_NAME
        });
        userWithOriginalName = await aidManagementClient.getUser();
        await validateLastRun();
    }));

    afterEach(willResolve(async () => {
        await aidManagementClient.updateUser(userWithOriginalName);
    }));

    it('updated user should be searchable', willResolve(async () => {
        if(!bail) {
            const oldNameSearchQuery = USER_BASIC;
            const newNameSearchQuery = Object.assign({}, USER_BASIC);
            const newName = 'Test ' + new Date();
            newNameSearchQuery.fullName = newName;

            LOG(`VERIFY that old name query "${oldNameSearchQuery.fullName}" succeeds`);
            await verifyUserSearchSucceeds(oldNameSearchQuery);

            // Update user name
            LOG(`UPDATE name to "${newName}"`);
            const newUser = Object.assign({}, userWithOriginalName);
            newUser.fullName = newName;
            await aidManagementClient.updateUser(newUser);

            LOG(`Wait for update and VERIFY that new name query "${newNameSearchQuery.fullName}" succeeds`);
            await waitForUserUpdateEventAndVerifyUser(newNameSearchQuery);

            LOG(`VERIFY that old name query "${oldNameSearchQuery.fullName}" fails`);
            await verifyUserSearchFails(oldNameSearchQuery);

            // Change name back
            LOG(`UPDATE name to "${userWithOriginalName.fullName}"`);
            await aidManagementClient.updateUser(userWithOriginalName);

            LOG(`Wait for update and VERIFY that old name query "${oldNameSearchQuery.fullName}" succeeds`);
            await waitForUserUpdateEventAndVerifyUser(oldNameSearchQuery);

            LOG(`VERIFY that new name query "${newNameSearchQuery.fullName}" fails`);
            await verifyUserSearchFails(newNameSearchQuery);
        }
    }));
});

async function verifyUserSearchSucceeds(searchQuery) {
    const usersFound = await xpsearchUserSearcherClient.searchByDisplayName(searchQuery, CLOUD_ID);
    validateUser(usersFound, searchQuery);
}

function validateUser(usersFound, searchQuery) {
    expect(usersFound.size).toEqual(1);
    expect(usersFound.items[0].content.account_id).toEqual(searchQuery.userId);
}

async function verifyUserSearchFails(searchQuery) {
    const usersFound = await xpsearchUserSearcherClient.searchByDisplayName(searchQuery, CLOUD_ID);
    expect(usersFound.size).toEqual(0);
}

async function waitForUserUpdateEventAndVerifyUser(searchQuery) {
    // Wait for the user updated event to be propagated through to the user search index
    let timeRemaining = 2000000;
    const delay = 10000;
    while (timeRemaining > 0) {
        const usersFound = await xpsearchUserSearcherClient.searchByDisplayName(searchQuery, CLOUD_ID);

        if(usersFound.size > 0) {
            validateUser(usersFound, searchQuery);
            LOG(`VERIFIED that name query "${searchQuery.fullName}" succeeds`);
            return;
        } else {
            await q.delay(delay);
            timeRemaining -= delay;
            LOG(`Time remaining to reach time limit is ${timeRemaining}`);
        }
    }
    LOG(`Time limit for Name query reached for "${searchQuery.fullName}"`);
}

async function updateOriginalName() {
    const curName = await aidManagementClient.getUser();
    LOG(`Current name of the user is "${curName.fullName}"`);
    userWithOriginalName.fullName = USER_BASIC.fullName;
    LOG(`Updating userWithOriginalName "${userWithOriginalName.fullName}"`);
    await aidManagementClient.updateUser(userWithOriginalName);
    await waitForUserUpdateEventAndVerifyUser(USER_BASIC);
    userWithOriginalName = await aidManagementClient.getUser();
    LOG(`Updated userWithOriginalName "${userWithOriginalName.fullName}"`);
}

async function validateLastRun() {
    if (userWithOriginalName.fullName !== USER_BASIC.fullName) {
        const names = userWithOriginalName.fullName.split(' ');

        if (names.length < 2) {
            await updateOriginalName();
        } else {
            const date_string = userWithOriginalName.fullName.substr(names[1].length  + 1);

            try {
                const old_date = Date.parse(date_string);
                LOG(`EPOC time in milliSeconds for old user "${old_date}`);

                const time_diff =  Date.now() - old_date;
                LOG(`Time diff between last user and current user is 
                    "${time_diff / ten_minutes_milliseconds}" minutes.`);

                if (time_diff > ten_minutes_milliseconds) {
                    LOG('Warning: something went wrong in last test run username did not update ' +
                        'back to original name. Updating username back to original name for current test.');

                    await updateOriginalName();
                } else {
                    LOG(`Current time diff "${time_diff / ten_minutes_milliseconds}" 
                        mins is less than 10 minutes. Exiting the test.`);
                    bail = true;
                }
            } catch (e) {
                LOG('Error: username does not match with original name and failed to parse the time stamp ' +
                    'Updating username back to original name for current test.');
                await updateOriginalName();
            }
        }
    }
}